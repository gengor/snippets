# Snippets #

This is my snippet collection for **[yas-snippets](https://github.com/joaotavora/yasnippet)**.

I usually place them in my Emacs or Spacemacs folder under `~/.emacs.d/snippets` or `~/.emacs.d/private/snippets`.

Notice that the markdown snippets for creating a metadata yaml head required you to have additional tools in place like Pandoc incl. special filters.
